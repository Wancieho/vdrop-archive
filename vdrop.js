/*
 *  Project: vDrop
 *  Description: 
 *  Author: Vaughan Webber - www.vaughanderful.co.za
 *  License: 
 *  Version: 1.0.0
 *  Dependancy: jquery-1.10.2 (jquery.com)
 *  Date: 17/08/2013
 */

;
(function($, window) {
	var pluginName = "vDrop", defaults = {
		onChange: '',
		theme: 'default',
		oneOpen: true
	};

	var dropdowns = [];

	function vDrop(element, options) {
		this.element = $(element);
		this.options = $.extend({}, defaults, options);

		this.__constructor();
	}

	vDrop.prototype = {
		__constructor: function() {
			var element = this.element;
			var scope = this;

			dropdowns.push(element);

			if (!element.hasClass('.dropdown'))
				element.addClass('dropdown');




			//find selected items text or if none currently selected select the first item
			var selectedText = element.find('li.selected').length > 0 ? element.find('li.selected').text() : element.find('li:first-child').addClass('selected').find('a').text();

			//create new selector for current selected select item
			element.prepend('<div class="selector">' + selectedText + '</div>');

			element.addClass(this.options.theme);

			var selector = $(this.element.find('div.selector'));

			//bind event to selector
			selector.on('click', function(event) {
				event.stopPropagation();

				$(this).blur();

				$(this).disableSelection();

				scope.expanded();

				if (scope.options.oneOpen)
					window.vDrop.hideAll($(scope.element.context).attr('id'));
			});







			element.append('<div class="arrow">&#9660;</div>');




			var selections = element.find('a');

			//bind event to selection items (dropdown list)
			selections.on('click', function(event) {
				event.preventDefault();

				$(this).blur();

				scope.select($(this));
			});

			$('html').click(function() {
				window.vDrop.hideAll();
			});
		},
		select: function(item) {
			this.expanded();

			var root = this.element.closest('.dropdown');

			//update selector text to match this clicked items text
			root.find('.selector').text(item.text());

			root.find('li').removeClass('selected');

			item.closest('li').addClass('selected');

			if (this.options.onChange !== '')
				this.options.onChange();
		},
		expanded: function() {
			var element = this.element;

			element.hasClass('expanded') ? element.removeClass('expanded').find('.arrow').html('&#9660;') : element.addClass('expanded').find('.arrow').html('&#9650;');
		}
	};

	$.fn.vDrop = function(options) {
		return this.each(function() {
			if (!$.data(this, "plugin_" + pluginName))
				$.data(this, "plugin_" + pluginName, new vDrop(this, options));
		});
	};

	$.fn.disableSelection = function() {
		return this.attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false);
	};

	vDrop.public = function() {
	}

	//global public methods
	vDrop.public.prototype = {
		hideAll: function(exclude) {
			$.each(dropdowns, function(i, dropdown) {
				if (exclude != 'undefined' && exclude != $(dropdown.context).attr('id'))
					dropdown.removeClass('expanded').find('.arrow').html('&#9660;');
			});
		}
	};

	window.vDrop = new vDrop.public();
})(jQuery, window);